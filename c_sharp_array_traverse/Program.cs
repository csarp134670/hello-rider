﻿using System;
using System.Diagnostics;

namespace c_sharp_array_traverse
{
    internal static class Program
    {
        public static void Main()
        {
            var inputs = Console.ReadLine()?.Split();
            Debug.Assert(inputs != null, nameof(inputs) + " != null");
            var count = 0;
            foreach (var word in inputs)
            {
                Console.WriteLine(word.ToUpper());
                count += 1;
            }

            Console.WriteLine("{0} words totally", count);
        }
    }
}