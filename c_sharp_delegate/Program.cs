﻿using System;

namespace c_sharp_delegate
{
    internal static class Program
    {
        private static void Play(OnDieDelegate onDie)
        {
            Console.WriteLine("做任务");
            Console.WriteLine("玩家正在战斗");
            Console.WriteLine("死亡");
            onDie?.Invoke();
        }

        private static void ShowDieUi()
        {
            Console.WriteLine("显示游戏死亡后的UI");
            Console.WriteLine("返回首页UI");
        }


        private static void Main()
        {
            Play(ShowDieUi);
        }

        private delegate void OnDieDelegate();
    }
}