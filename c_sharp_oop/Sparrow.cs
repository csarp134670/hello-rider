namespace c_sharp_oop
{
    public class Sparrow : AnimalAbstract, IFlying
    {
        public Sparrow(string name) : base(name)
        {
        }

        public string FlightAltitude()
        {
            return "一点点高";
        }

        public override string AnimalSound()
        {
            return "啾啾！啾啾啾！！";
        }
    }
}