namespace c_sharp_oop
{
    public class Duck : AnimalAbstract, IFlying, ISwimming
    {
        public Duck(string name) : base(name)
        {
        }

        public string FlightAltitude()
        {
            return "飞来飞去到处飞";
        }

        public string SwimmingSpeed()
        {
            return "摇摇晃晃超快的哦";
        }

        public override string AnimalSound()
        {
            return "嘎嘎！嘎嘎嘎！！";
        }
    }
}