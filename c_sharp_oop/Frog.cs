namespace c_sharp_oop
{
    public class Frog : AnimalAbstract, ISwimming
    {
        public Frog(string name) : base(name)
        {
        }

        public string SwimmingSpeed()
        {
            return "一点点慢";
        }

        public override string AnimalSound()
        {
            return "呱呱！呱呱呱！！";
        }
    }
}