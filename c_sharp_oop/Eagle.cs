namespace c_sharp_oop
{
    public class Eagle : AnimalAbstract, IFlying
    {
        public Eagle(string name) : base(name)
        {
        }

        public string FlightAltitude()
        {
            return "很高很高！";
        }

        public override string AnimalSound()
        {
            return "嗷嗷！嗷嗷嗷！！";
        }
    }
}