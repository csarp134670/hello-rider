﻿using System;
using System.Collections.Generic;

namespace c_sharp_oop
{
    internal static class Program
    {
        public static void Main()
        {
            var animalList = new List<AnimalAbstract>
            {
                new Duck("鸭鸭一号"),
                new Duck("鸭鸭二号"),
                new Frog("小树蛙"),
                new Eagle("老鹰"),
                new Sparrow("小麻雀")
            };
            animalList.ForEach(SelfIntroduce);
        }

        private static void SelfIntroduce(AnimalAbstract animal)
        {
            Console.WriteLine("~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#");
            Console.WriteLine("大家好，我叫{0}", animal.Name());
            Console.WriteLine(animal.AnimalSound());
            if (animal is IFlying flyingAnimal) CanFly(flyingAnimal);
            if (animal is ISwimming swimmingAnimal) CanSwim(swimmingAnimal);
        }

        private static void CanFly(IFlying animal)
        {
            Console.WriteLine("我会飞！我飞得{0}", animal.FlightAltitude());
        }

        private static void CanSwim(ISwimming animal)
        {
            Console.WriteLine("我会游！我游得{0}", animal.SwimmingSpeed());
        }
    }
}