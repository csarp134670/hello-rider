namespace c_sharp_oop
{
    public interface IFlying
    {
        string FlightAltitude();
    }
}