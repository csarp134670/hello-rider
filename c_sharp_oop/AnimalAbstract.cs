namespace c_sharp_oop
{
    public abstract class AnimalAbstract
    {
        private readonly string _name;

        protected AnimalAbstract(string name)
        {
            _name = name;
        }

        public string Name()
        {
            return _name;
        }

        public abstract string AnimalSound();
    }
}