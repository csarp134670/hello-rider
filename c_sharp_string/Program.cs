﻿using System;

namespace c_sharp_string
{
    internal static class Program
    {
        public static void Main()
        {
            Console.WriteLine("## C:\text.txt ##");
            Console.WriteLine("## C:\\text.txt ##");
            Console.WriteLine(@"## C:\text.txt ##"); //字符串前面的@可以让它取消转义
            var str = @"(start)
name=""pony"",
age=22
"; //这种方式甚至支持多行字符串模式，如果想要使用"，则需要两个"来表示一个
            str += "(end)\n"; //字符串拼接
            Console.WriteLine(str);
        }
    }
}