﻿using System;
using System.Collections.Generic;
using System.IO;

namespace c_sharp_io
{
    internal static class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length == 0) return;
            switch (args[0])
            {
                case "mkdir":
                    Mkdir(args);
                    break;
                case "ls":
                    Ls();
                    break;
                case "cat":
                    Cat(args[1]);
                    break;
                case "mv":
                    Mv(args[1], args[2]);
                    break;
                case "cp":
                    Cp(args[1], args[2]);
                    break;
                case "rm":
                    Rm(args[1]);
                    break;
                case "write":
                    Write(args[1], args[2]);
                    break;
                case "append":
                    Append(args[1], args[2]);
                    break;
                default:
                    Console.WriteLine("invalid command{0}", args[0]);
                    break;
            }
        }

        private static void Mkdir(IReadOnlyList<string> args)
        {
            for (var i = 1; i < args.Count; i++) Directory.CreateDirectory(args[i]);
        }

        private static void Ls()
        {
            var wd = new DirectoryInfo(Environment.CurrentDirectory);
            Console.WriteLine(wd.GetDirectories());
        }

        private static void Cat(string file)
        {
            Console.WriteLine(File.ReadAllText(file));
        }

        private static void Mv(string src, string dst)
        {
            Directory.Move(src, dst);
        }

        private static void Cp(string src, string dst)
        {
            File.Copy(src, dst);
        }

        private static void Rm(string path)
        {
            if (Directory.Exists(path))
                Directory.Delete(path, true);
            else if (File.Exists(path)) File.Delete(path);
        }

        private static void Write(string path, string content)
        {
            File.WriteAllText(path, content);
        }

        private static void Append(string path, string content)
        {
            File.AppendAllText(path, content);
        }
    }
}
/*
 mkdir
 ls
 cat
 mv
 cp
 rm

 */