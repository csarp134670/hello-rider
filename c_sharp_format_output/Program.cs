﻿using System;

namespace c_sharp_format_output
{
    internal static class Program
    {
        public static void Main()
        {
            //two sum
            Console.WriteLine("请在一行内输入a和b两个整数");
            var values = Console.ReadLine()?.Split();
            var a = Convert.ToInt32(values?[0]);
            var b = Convert.ToInt32(values?[1]);
            Console.WriteLine("{0}+{1}={2}", a, b, a + b);
        }
    }
}