﻿using System;

namespace c_sharp_input
{
    /**
     * 猜数游戏
     * @author LuoHao
     */
    internal static class Program
    {
        public static void Main()
        {
            var score = 0;
            var rand = new Random();
            var randNumber = rand.Next(1, 101);
            Console.WriteLine("请猜一个1~100的整数，按回车退出。当前得分:" + score);
            var line = Console.ReadLine();
            while (line != null && !line.Equals(""))
            {
                try
                {
                    var guessed = Convert.ToInt32(line);
                    if (guessed < randNumber)
                    {
                        Console.WriteLine("太小了，请再猜大一点，按回车退出。当前得分:" + score);
                    }
                    else if (guessed > randNumber)
                    {
                        Console.WriteLine("太大了，请再猜小一点，按回车退出。当前得分:" + score);
                    }
                    else
                    {
                        score += 1;
                        Console.WriteLine("猜对了，请猜下一个整数吧！按回车退出。当前得分:" + score);
                        randNumber = rand.Next(1, 101);
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("请猜一个1~100的整数，按回车退出。当前得分:" + score);
                }

                line = Console.ReadLine();
            }

            Console.WriteLine("您的总分是{0:N0}，实力通天，势不可挡！", score);
        }
    }
}